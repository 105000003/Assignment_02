var startState = { create: function() {
    // Add a background image 
    game.add.image(0, 0, 'background_menu'); 
    game.add.image(20, 80, 'logo'); 
    // Display the name of the game 
    //var nameLabel = game.add.text(game.width/2, 80, 'Shadow Stairs', { font: '50px Arial', fill: '#000000' }); nameLabel.anchor.setTo(0.5, 0.5);
    // Show the score at the center of the screen 
    var scoreLabel = game.add.text(game.width/2, game.height/2, "P to Pause, R to Return to game", { font: '20px Passion One', fill: '#8B0000' }); 
    scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game 
    var startLabel = game.add.text(game.width/2, game.height-80, 'press enter to start', { font: '40px Passion One', fill: '#FF0000' }); 
    startLabel.anchor.setTo(0.5, 0.5); 
    // Create a new Phaser keyboard variable: the up arrow key // When pressed, call the 'start'
    var enterkey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER); 
    enterkey.onDown.add(this.start_1, this); 
}, start_1: function() { 
    // Start the actual game 
    game.state.start('play'); }, 
}; 

