var loadState = { preload: function () {
    // Add a 'loading...' label on the screen 
    var loadingLabel = game.add.text(game.width/2, 150, 'loading...', { font: '30px Arial', fill: '#000000' }); 
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar 
    var progressBar = game.add.sprite(game.width/2, 200, 'progressBar'); 
    progressBar.anchor.setTo(0.5, 0.5); 
    game.load.setPreloadSprite(progressBar);
    // Load all game assets 
    game.load.spritesheet('player', 'player.png', 32, 32);
    game.load.image('wall', 'wall.png');
    game.load.image('ceiling', 'ceiling.png');
    game.load.image('normal', 'normal.png');
    game.load.image('nails', 'nails.png');
    game.load.spritesheet('conveyorRight', 'conveyor_right.png', 96, 16);
    game.load.spritesheet('conveyorLeft', 'conveyor_left.png', 96, 16);
    game.load.spritesheet('trampoline', 'trampoline.png', 96, 22);
    game.load.spritesheet('fake', 'fake.png', 96, 36);
    game.load.spritesheet('heart', 'heart.png', 32, 32);
    game.load.image('background', 'background.png',400,400);
    // Load a new asset that we will use in the menu state 
    game.load.image('background_menu', 'background_menu.png');
    game.load.image('logo', 'logo.png',250,50);
    game.load.image('logo_2', 'logo_2.png',250,50);
}, create: function() { 
    // Go to the menu state 
    game.state.start('start_menu'); } 
}; 