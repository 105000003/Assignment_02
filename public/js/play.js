var player;
var keyboard;
var yum;

var platforms = [];
var leftWall;
var rightWall;
var ceiling;

var text1;
var text2;
var run;
var form;

//var distance = 0;
//var status = 'running';

var playState = {
    preload: function () {
        game.load.audio('yum', 'yum.wav');
    },

    create: function () {
        yum = game.add.audio('yum');
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D,
            'p': Phaser.Keyboard.P,
            'r': Phaser.Keyboard.R,
        });
        game.global.score = 0;
        run = true;
        createBounders();
        createPlayer();
        createTextsBoard();
    },
    update: function () {
        // bad
        //if (status == 'gameOver' && keyboard.enter.isDown) restart();
        //if (status != 'running') return;
        if (keyboard.p.isDown && (run == true)) {
            run = false;
            player.body.gravity.y = 0;
            player.body.velocity.y = 0;
        }
        if (keyboard.r.isDown && (run == false)) {
            run = true;
            player.body.gravity.y = 500;
        }
        if (run == true) {
            this.physics.arcade.collide(player, platforms, effect);
            this.physics.arcade.collide(player, [leftWall, rightWall]);
            checkTouchCeiling(player);
            checkGameOver();

            updatePlayer();
            updatePlatforms();
            updateTextsBoard();

            createPlatforms();
        }
    }
}


var background;
function createBounders() {
    background = game.add.sprite(0, 0, 'background');
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(365, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.image(0, 0, 'ceiling');
}

var lastTime = 0;
function createPlatforms() {
    if (game.time.now > lastTime + 600) {
        lastTime = game.time.now;
        createOnePlatform();
        //distance += 1;
        game.global.score += 1;
    }
}

function createOnePlatform() {

    var platform;
    //var addlife;
    var x = Math.random() * (400 - 96 - 40) + 20;
    var y = 400;
    var rand = Math.random() * 110;

    if (rand < 20) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else if (rand < 90) {
        platform = game.add.sprite(x, y, 'heart');
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }
    //game.physics.arcade.enable(addlife); 
    //--------
    /*game.physics.arcade.overlap(player, addlife, this.takeCoin, null, this);
    function takeCoin(player, heart) { 
        heart.kill(); // Kill the coin.    
        player.score += 5;    
    };*/
    //---------
    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createPlayer() {
    player = game.add.sprite(200, 50, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createTextsBoard() {
    var style = { fill: '#ff0000', fontSize: '20px' }
    var style_2 = { fill: '#000000', fontSize: '40px' }
    text1 = game.add.text(40, 10, '', style);
    text2 = game.add.text(250, 10, '', style);
    /*text4 = game.add.text(40, 140, 'You Have Died !!!', style_2);
    text3 = game.add.text(48, 190, 'Enter To Restart', style_2);
    text3.visible = false;
    text4.visible = false;*/
}

function updatePlayer() {
    if (keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if (keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    setPlayerAnimate(player);
}

function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
        player.frame = 8;
    }
}

function updatePlatforms() {
    for (var i = 0; i < platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if (platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard() {
    text1.setText('LIFE:' + player.life);
    text2.setText('SCORE:' + game.global.score);
}

function effect(player, platform) {
    if (platform.key == 'conveyorRight') {
        conveyorRightEffect(player, platform);
    }
    if (platform.key == 'conveyorLeft') {
        conveyorLeftEffect(player, platform);
    }
    if (platform.key == 'trampoline') {
        trampolineEffect(player, platform);
    }
    if (platform.key == 'nails') {
        nailsEffect(player, platform);
    }
    if (platform.key == 'normal') {
        basicEffect(player, platform);
    }
    if (platform.key == 'fake') {
        fakeEffect(player, platform);
    }
    if (platform.key == 'heart') {
        heartEffect(player, platform);
    }
}

function heartEffect(player, platform) {
    yum.play();
    player.life += 5;
    platform.kill();
}

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 3;
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if (player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if (player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function () {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}

function checkTouchCeiling(player) {
    if (player.body.y < 0) {
        if (player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if (game.time.now > player.unbeatableTime) {
            player.life -= 3;
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}

function checkGameOver() {
    if (player.life <= 0 || player.body.y > 500) {
        gameOver();
    }
}

function gameOver() {
    platforms.forEach(function (s) { s.destroy() });
    platforms = [];
    game.state.start('menu');
    /*text3.visible = true;
    text4.visible = true;*/
    /*platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    status = 'gameOver';*/
}

/*function restart () {
    text3.visible = false;
    text4.visible = false;
    distance = 0;
    createPlayer();
    status = 'running';
}*/