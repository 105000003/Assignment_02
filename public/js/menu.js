var menuState = { create: function() {
    // Add a background image 
    game.add.image(0, 0, 'background_menu'); 
    game.add.image(20, 80, 'logo_2'); 
    // Display the name of the game 
    //var nameLabel = game.add.text(game.width/2, 80, 'Shadow Stairs', { font: '50px Arial', fill: '#000000' }); nameLabel.anchor.setTo(0.5, 0.5);
    // Show the score at the center of the screen 
    var scoreLabel = game.add.text(game.width/2, game.height/2, 'Score: ' + game.global.score, { font: '40px Passion One', fill: '#8B0000' }); 
    scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game 
    var startLabel = game.add.text(game.width/2, game.height-80, 'press enter to retry', { font: '40px Passion One', fill: '#FF0000' }); 
    startLabel.anchor.setTo(0.5, 0.5); 
    var startLabel_2 = game.add.text(game.width/2, game.height-100, 'press Q to quit', { font: '20px Passion One', fill: '#FF0000' }); 
    startLabel_2.anchor.setTo(0.5, 0.5); 
    // Create a new Phaser keyboard variable: the up arrow key // When pressed, call the 'start'
    var enterkey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER); 
    var Q = game.input.keyboard.addKey(Phaser.Keyboard.Q); 
    enterkey.onDown.add(this.start, this); 
    Q.onDown.add(this.END, this); 
}, start: function() { 
    // Start the actual game 
    game.state.start('play'); 
  },END: function(){
    game.state.start('start_menu'); 
  } 
};

//all variables
var messageList;
var messageInput;
var submitButton;
var MESSAGE_TEMPLATE =
  '<div class="message-container">' +
  '<div class="spacing"></div>' +
  '<div class="message"></div>'
  '</div>';



window.onload = function () {
    checkSetup();
    messageList = document.getElementById('messages');
    messageInput = document.getElementById('message');
    submitButton = document.getElementById('submit');

    submitButton.addEventListener('click', saveMessage, false);

    var buttonTogglingHandler = toggleButton;
    messageInput.addEventListener('keyup', buttonTogglingHandler);
    messageInput.addEventListener('change', buttonTogglingHandler);
    loadMessages();
}

// Loads chat messages history and listens for upcoming ones.
function loadMessages() {
  var messagesRef = firebase.database().ref();
  messagesRef.off();
  var setMessage = function (data) {
    var val = data.val();
    displayMessage(data.key, val.name, val.text);
  };
  messagesRef.limitToLast(50).orderByChild("des").on('child_added', setMessage);
  messagesRef.limitToLast(50).orderByChild("des").on('child_changed', setMessage);
};

// Saves a new message on the Firebase DB.
function saveMessage() {
  if (messageInput.value) {
    var messagesRef = firebase.database().ref();
    messagesRef.push({
      name: messageInput.value,
      text: game.global.score,
      des: 99999999999 - game.global.score
    }).then(function () {
      resetMaterialTextfield(messageInput);
      toggleButton();
    }).catch(function (error) {
      console.error('Error writing new message to Firebase Database', error);
    });
  }
};

// Resets the given MaterialTextField.
function resetMaterialTextfield(element) {
  element.value = '';
  element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
};

// Displays a Message in the UI.
function displayMessage(key, name, text) {
  var div = document.getElementById(key);
  // If an element for that message does not exists yet we create it.
  if (!div) {
    var container = document.createElement('div');
    container.innerHTML = MESSAGE_TEMPLATE;
    div = container.firstChild;
    div.setAttribute('id', key);
    messageList.appendChild(div);
  }
  var messageElement = div.querySelector('.message');
  if (text) { // If the message is text.
    messageElement.textContent = name + " : " + text;
    messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
  } 
  setTimeout(function () { div.classList.add('visible') }, 1);
  messageList.scrollTop = messageList.scrollHeight;
  messageInput.focus();
};

function toggleButton() {
  var mi = document.getElementById('message');
  var sb = document.getElementById('submit');
  if (mi.value) {
    sb.removeAttribute('disabled');
  } else {
    sb.setAttribute('disabled', 'true');
  }
};

function checkSetup() {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
    window.alert('You have not configured and imported the Firebase SDK. ' +
      'Make sure you go through the codelab setup instructions and make ' +
      'sure you are running the codelab using `firebase serve`');
  }
};