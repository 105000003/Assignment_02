# Software Studio 2018 Spring Assignment 02 小朋友下樓

## deployed with firebase :  https://phasergame-81192.firebaseapp.com/
## gitlab page(not stable): https://105000003.gitlab.io/Assignment_02

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| Finished ->A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Finished ->Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Finished ->Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Finished ->Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Finished ->Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Finished ->Appearance (subjective)                                                                        |  10%  |
| Finished ->Other creative features in your game (describe on README.md)                                   |  10%  |

# interesting traps or special mechanisms:
* normal block
* spring block(character bounces upon impact)
* spike block(character looses health)
* fake block(character falls through)
* heart(collectible, character gains 5 health)

# additional sound effects and UI
* upon collecting hearts, character says "yum"
* UI displays instructions

# Other creative features
* you can pause in the game
* leaderboard shows scores from highest to lowest
* don't have to register to save score

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed
